import { Component } from '@angular/core';

import { AnnouncementsPage } from '../announcements/announcements';
import { SermonsPage } from '../sermons/sermons';
import { MorePage } from '../more/more';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = AnnouncementsPage;
  tab2Root: any = SermonsPage;
  tab3Root: any = MorePage;

  constructor() {

  }
}
