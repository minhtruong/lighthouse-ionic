import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';
import { Sermon } from '../../models/sermon';
import { MusicControls, MusicControlsOptions } from 'ionic-native';
import { Platform } from 'ionic-angular';

@Component({
  selector: 'page-player',
  templateUrl: 'player.html'
})
export class PlayerPage {
  sermon:Sermon;
  stream:any;
  promise:any;
  isPlaying:boolean;
  options:MusicControlsOptions;
  audio:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform) {
    this.sermon = navParams.data;      
    this.stream = new Audio('https:' + this.sermon.audio);
    this.audio = document.getElementById('audio');
    // if(this.platform.is('android') || this.platform.is('ios')) {
    //   MusicControls.create({
    //     track: this.sermon.title,
    //     artist: this.sermon.who,
    //     hasNext: false,
    //     hasPrev: false,
    //     dismissable: true,
    //     isPlaying: false,
    //     cover: '/assets/imgs/lh_logo.png',
    //     hasClose: false,
    //     ticker: this.sermon.title
    //   });
    //   MusicControls.subscribe().subscribe(action => {
    //     switch(action) {          
    //         case 'music-controls-pause':
    //             this.isPlaying = false;
    //             this.stream.pause();
    //             MusicControls.updateIsPlaying(false);
    //             break;
    //         case 'music-controls-play':
    //             this.isPlaying = true;
    //             this.stream.play();
    //             MusicControls.updateIsPlaying(true);
    //             break;
    //         case 'music-controls-destroy':
    //             this.isPlaying = false;
    //             this.stream.pause();
    //             break;
    //         default:
    //             break;
    //     }      
    //   });
    //   MusicControls.listen();
    // }
  }  

  play() {
    this.isPlaying = true;
    MusicControls.updateIsPlaying(true);
    this.stream.play();
    this.promise = new Promise((resolve,reject) => {
      this.stream.addEventListener('playing', () => {
        resolve(true);
      });

      this.stream.addEventListener('error', () => {
        reject(false);
      });
    });
   
    return this.promise;
  }

  pause() {
    this.isPlaying = false;
    this.stream.pause();
    MusicControls.updateIsPlaying(false);
  };

}
