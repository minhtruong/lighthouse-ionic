import { Component } from '@angular/core';
import { Http } from '@angular/http';

import { NavController } from 'ionic-angular';
import { Announcement } from '../../models/announcement';
import * as marked from 'marked';

@Component({
  selector: 'page-announcements',
  templateUrl: 'announcements.html'
})
export class AnnouncementsPage {
  list:Announcement[];
  private md: MarkedStatic;

  constructor(public http: Http, public navCtrl: NavController) {
    this.md = marked.setOptions({});
    this.getAnnouncements(null);
  }

  getAnnouncements(refresher) {
    // this.loading.present();
    // https://cdn.contentful.com/spaces/5ur1xyixb1uc/entries/?content_type=announcement&access_token=1e277caaf52222d9705ee4af69c812fe25140f574a8fe0f804ac8986c25d3955    
    this.http.get('https://cdn.contentful.com/spaces/5ur1xyixb1uc/entries/?content_type=events&access_token=862f5ad382b78783b55b80c350cba412dff0cbd8fedd294e8edc8b58334895d7')    
      .subscribe(data => {                
          let items = data.json().items[0].fields.announcements;
          this.list = [];
          for(let i = 0; i < items.length; i++) {
            for(let j = 0; j < data.json().includes.Entry.length; j++)   {
              let entry = data.json().includes.Entry[j];
              if(items[i].sys.id == entry.sys.id) {
                this.list.push(new Announcement(entry.fields.title, this.md.parse(entry.fields.description)));
                j = data.json().includes.Entry.length;
              }
            }            
          }
          // this.loading.dismiss();
          if(refresher) refresher.complete();
        }, error => {
          console.log(JSON.stringify(error.json()));
        });
  }
}
