import { Component } from '@angular/core';
import { Http } from '@angular/http';

import { NavController } from 'ionic-angular';
import { Sermon } from '../../models/sermon';
import { PlayerPage } from '../player/player';

@Component({
  selector: 'page-sermons',
  templateUrl: 'sermons.html'
})
export class SermonsPage {
  list:Sermon[]; 

  constructor(public http: Http, public navCtrl: NavController) {
    this.getSermons(null);
  }

  playAudio(item) {
    this.navCtrl.push(PlayerPage, item);
  }

  getSermons(refresher) {
    this.http.get('https://cdn.contentful.com/spaces/5ur1xyixb1uc/entries/?order=-fields.date&include=1&content_type=sermon&access_token=1e277caaf52222d9705ee4af69c812fe25140f574a8fe0f804ac8986c25d3955')
      .subscribe(data => {                          
          let items = data.json().items;
          let assets = data.json().includes.Asset;
          this.list = [];
          console.log(assets);
          for(let i = 0; i < items.length; i++) {
            let item = items[i].fields;
            let date = item.date;;
            let title = item.title;
            let who = item.who;
            let description = item.description;
            let imageId;
            if(item.hasOwnProperty('image')) {
              imageId = item.image.sys.id;
            }
            let audioId;
            if(item.hasOwnProperty('audio')) {
              audioId = item.audio.sys.id;
            }
            let image;
            let audio;            
            for(let j = 0; j < assets.length; j++) {
              let asset = assets[j]
              if(imageId == asset.sys.id) {
                image = asset.fields.file.url;
              }
              if(audioId == asset.sys.id) {
                audio = asset.fields.file.url;
              }
            }
            this.list.push(new Sermon(date, title, who, description, image, audio));
          }
          if(refresher) refresher.complete();
        }, error => {
          console.log(JSON.stringify(error.json()));
        });
  }

}
