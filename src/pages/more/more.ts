import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';

import { Link } from '../../models/link';

/*
  Generated class for the More page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-more',
  templateUrl: 'more.html'
})
export class MorePage {
  list:Link[]; 

  constructor(public http: Http, public navCtrl: NavController, public navParams: NavParams) {
    this.getLinks();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MorePage');
  }

  openLink(item) {
    console.log(item);
    window.open(item.url, '_system');
  }

  getLinks() {
    this.http.get('https://cdn.contentful.com/spaces/5ur1xyixb1uc/entries/?include=1&content_type=moreLinkList&access_token=1e277caaf52222d9705ee4af69c812fe25140f574a8fe0f804ac8986c25d3955')
      .subscribe(data => {                          
          let items = data.json().includes.Entry;
          this.list = [];
          for(let i = 0; i < items.length; i++) {
            let item = items[i];
            this.list.push(new Link(item.fields.title, item.fields.description, item.fields.link));
          }
          console.log(items);
        }, error => {
          console.log(JSON.stringify(error.json()));
        });
  }
}
