import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { AnnouncementsPage } from '../pages/announcements/announcements';
import { SermonsPage } from '../pages/sermons/sermons';
import { PlayerPage } from '../pages/player/player';
import { TabsPage } from '../pages/tabs/tabs';
import { MorePage } from '../pages/more/more';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    AnnouncementsPage,
    SermonsPage,
    PlayerPage,
    TabsPage,
    MorePage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    AnnouncementsPage,
    SermonsPage,
    PlayerPage,
    TabsPage,
    MorePage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
