export class Sermon {
    constructor(public date: string, public title: string, public who: string, public description: string, public image: string, public audio: string) {

    }
}