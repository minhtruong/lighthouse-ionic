export class Link {
    constructor(public title: string, public description: string, public url: string) {

    }
}